.. |label| replace:: Rechnungen und Dokumente im Kundenkonto
.. |snippet| replace:: FvPDFSaver
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.2.0
.. |maxVersion| replace:: 5.7.7
.. |version| replace:: 1.2.1
.. |php| replace:: 7.4


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Das Plugin ermöglicht die Darstellung der Sage 100 Belege, die vom 1st Vision Sage 100-Modul „PDF-Saver“ erstellt und von OLSI auf den Webserver hochgeladen wurden, im Kundenkonto des Webshops. 
Prinzipiell können alle Belegarten der Sage 100 erstellt und im Kundenkonto dargestellt werden. 


Frontend
--------
Nach Installation  und Aktivierung des Plugins erscheint ein neuer Menüpunkt „Dokumente“ im Shopware Kundenkonto.

.. image:: FvPDFSaver1.png

Bei Aufruf gelangt man auf den Frontend-Plugin-Controller „FvPdfSaver“. Hier werden in Echtzeit die hochgeladenen Belege im konfigurierten PDF-Verzeichnis auf dem Webserver gescannt und alle Belege des Kunden nach Datum sortiert ausgegeben. Informationsträger der dargestellten Daten ist hierbei ausschließlich der Dateiname der PDF-Belege.

.. image:: FvPDFSaver2.png


Backend
-------
Konfiguration
_____________
.. image:: FvPDFSaver4.png
:Pfad zu PDF-Dokumenten: Hier kann der Pfad ausgehend vom Shopware-Root-Verzeichnis angegeben werden, in den die PDF-Belege liegen.
:PDF-Dateinamen: Der Syntax der PDF wird hier hinterlegt.
:Versende Benachrichtigungs-Mails: Bei Ja wird eine eMail an den Kunden gesendet sobald eine neue PDF für ihn bereit gestellt wurde.
:Benachrichtungen für diese Belegarten: Hier müssen die Bezeichnungen der Beleg-Art hinterlegt werden mit Komma getrennt

FTP-Pfad
________
.. image:: FvPDFSaver3.png
Im Standard werden die Belege so in das FTP Verzeichnis abgelegt. 

Cron-Job
________
Es werden Cron-Jobs angelegt mit der Bezeichnung
:PDF Belege indizieren: Dieser Cronjob indexiert die Belege in dem hinterlegten Verzeichnis
:Benachrichtigungen für PDF-Belege versenden: Dieser Cronjob versendet die eMails an den Kunden, das ein neue Beleg in seinem Konto erschienen ist

Email-Vorlage
_____________
:System-E-Mails: sORDERDOCUMENTS:

technische Beschreibung
------------------------
Bei dem PDF-Dateinamen werden folgende Variablen benützt:
%DEBNO% 	Kundennummer
%DOCTYPE%	Beleg-Art (wird 1:1 so im Frontend ausgegeben)
%REFNO%		Beleg-Nummer, z.B. Rechnungsnummer
%DATE%		Erstelldatum des Belegs im Format YYYYMMDD


Modifizierte Template-Dateien
-----------------------------
:/account/sidebar.tpl:



